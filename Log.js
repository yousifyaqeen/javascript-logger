const callerId = require('caller-id');
const path = require('path');
const winston = require('winston');
const { combine, printf } = winston.format;

const customLevels = {
    levels: {
        ERROR: 0,
        WARN: 1,
        SUCC: 1,
        INFO: 3,
        HTTP: 4,
        CLIENT: 5,
        DB: 6,
        ACCESS: 7,
        EMAIL: 8,
        CRITICAL: 9,
        PYTHON: 9
    },
    colors: {
        ERROR: 'red',
        WARN: 'yellow',
        SUCC: 'green',
        INFO: 'white',
        HTTP: 'blue',
        CLIENT: 'red',
        DB: 'cyan',
        ACCESS: 'green',
        EMAIL: 'green',
        CRITICAL: 'red',
        PYTHON: 'blue'
    }
};

winston.addColors(customLevels.colors);

const myFormat = printf(({ level, message, source, timestamp }) => {
    return `${timestamp} [${source}] [${level}]: ${message}`;
});

const options = {
    level: 'PYTHON',
    levels: customLevels.levels,
    format: combine(
        winston.format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
        winston.format.json()
    ),
    transports: [
        new winston.transports.File({ filename: path.resolve(path.join(__dirname, '/../logs/server.log')), timestamp: true }),

        new winston.transports.Console(
            {
                format: winston.format.combine(
                    winston.format.colorize(),
                    winston.format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
                    myFormat
                )
            })
    ]
};

const Infologger = winston.createLogger(options);

const log = {
    info: function (message) {
        const caller = callerId.getData();
        Infologger.log('INFO', message, { source: `${caller.filePath} : ${caller.lineNumber}` });
    },
    warning: function (message) {
        const caller = callerId.getData();
        Infologger.log('WARN', message, { source: `${caller.filePath} : ${caller.lineNumber}` });
    },
    error: function (message) {
        const caller = callerId.getData();
        Infologger.log('ERROR', message, { source: `${caller.filePath} : ${caller.lineNumber}` });
    },
    success: function (message) {
        const caller = callerId.getData();
        Infologger.log('SUCC', message, { source: `${caller.filePath} : ${caller.lineNumber}` });
    },
    http: function (message) {
        const caller = callerId.getData();
        Infologger.log('HTTP', message, { source: `${caller.filePath} : ${caller.lineNumber}` });
    },
    database: function (message) {
        const caller = callerId.getData();
        Infologger.log('DB', message, { source: `${caller.filePath} : ${caller.lineNumber}` });
    },
    access: function (ip, user) {
        const caller = callerId.getData();
        if (user) {
            Infologger.log('ACCESS', `User ${user} from ${ip}`, { source: `${caller.filePath} : ${caller.lineNumber}` });
        } else {
            Infologger.log('ACCESS', `${ip}`, { source: `${caller.filePath} : ${caller.lineNumber}` });
        }
    },
    email: function (emailInfo) {
        const caller = callerId.getData();
        Infologger.log('EMAIL', emailInfo, { source: `${caller.filePath} : ${caller.lineNumber}` });
    },
    client: function (message) {
        const caller = callerId.getData();
        Infologger.log('CLIENT', message, { source: `${caller.filePath} : ${caller.lineNumber}` });
    },
    critical: function (message) {
        const caller = callerId.getData();
        Infologger.log('CRITICAL', message, { source: `${caller.filePath} : ${caller.lineNumber}` });
    },
    python: function (message) {
        const caller = callerId.getData();
        Infologger.log('PYTHON', message, { source: `${caller.filePath} : ${caller.lineNumber}` });
    },
    getLogs: function (req, res) {
        if (!req) {
            // no host
            return;
        }
        // If no username and password is setup in the environement
        if (process.env.LOG_ADMIN_USERNAME == null || process.env.LOG_ADMIN_PASSWORD == null) {
            res.status(501).send('Unimplemented');
            return;
        }
        // -----------------------------------------------------------------------
        // authentication middleware
        const auth = { login: process.env.LOG_ADMIN_USERNAME, password: process.env.LOG_ADMIN_PASSWORD };

        // parse login and password from headers
        const b64auth = (req.headers.authorization || '').split(' ')[1] || '';
        const [login, password] = Buffer.from(b64auth, 'base64').toString().split(':');

        // Verify login and password are set and correct
        if (!login || login !== auth.login || !password || password !== auth.password) {
            // Access denied...
            res.set('WWW-Authenticate', 'Basic realm="401"'); // change this
            res.status(401).send('Authentication required.'); // custom message
            return;
            // Access granted...
        }
        // -----------------------------------------------------------------------
        const options = {
            from: new Date() - (24 * 60 * 60 * 1000) * 30,
            until: new Date(),
            limit: 100000,
            start: 0,
            order: 'desc'
        };
        //
        // Find items logged between today and yesterday.
        //
        Infologger.query(options, function (err, results) {
            if (err) {
                console.error(err);
                res.send(500, 'InternalError');
                return;
            }
            res.send(results);
        });
    }
};

module.exports = log;
